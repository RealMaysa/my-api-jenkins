from flask import Flask

app = Flask(__name__)

@app.route('/')
def index():
    return "Index!"

@app.route('/getcode', methods = ['GET'])
def getcode():
    return "Hello World!"

@app.route('/plus/<num1>/<num2>', methods = ['GET'])
def plus (num1, num2):
    #print(type(num1),type(num2))
    try:
        num1 = int(num1)
        num2 = int(num2)
        result = str(num1 + num2)
    except:
        result = 'error, input must be number.'
    
    return result

if __name__  == '__main__':
    app.run()